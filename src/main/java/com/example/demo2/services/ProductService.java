package com.example.demo2.services;

import com.example.demo2.entities.Product;

public interface ProductService {

	public Iterable<Product> findAll();
	
}
