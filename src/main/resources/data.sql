DROP TABLE IF EXISTS product;

CREATE TABLE product (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name varchar(250) NOT NULL,
  price decimal(10,1) NOT NULL,
  quantity int(11) NOT NULL,
  description text NOT NULL,
  status tinyint(1) NOT NULL
);

  
INSERT INTO product (name, price, quantity, description, status) VALUES('Mobile 2', '1.0', 5, 'description 2', 1);
INSERT INTO product (name, price, quantity, description, status) VALUES('Mobile 1', '2.0', 2, 'description 1', 1);
INSERT INTO product (name, price, quantity, description, status) VALUES('Mobile 3', '3.0', 9, 'description 3', 1);
INSERT INTO product (name, price, quantity, description, status) VALUES('Computer 1', '5.0', 12, 'description 4', 0);
INSERT INTO product (name, price, quantity, description, status) VALUES('Computer 2', '7.0', 5, 'description 5', 1);
INSERT INTO product (name, price, quantity, description, status) VALUES('Computer 3', '12.0', 2, 'description 6', 1);
INSERT INTO product (name, price, quantity, description, status) VALUES('Laptop 1', '3.0', 8, 'description 7', 0);
INSERT INTO product (name, price, quantity, description, status) VALUES('Laptop 2', '4.0', 11, 'description 8', 0);
INSERT INTO product (name, price, quantity, description, status) VALUES('Laptop 3', '2.0', 15, 'description 9', 1);